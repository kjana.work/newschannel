import { Injectable } from '@angular/core';
import { BaseServicesService } from './base-services.service';
import { Observable } from 'rxjs';
import { environment } from "src/environments/environment";
@Injectable({
  providedIn: 'root'
})
export class AdminServiceService {
  
  constructor(private baseService: BaseServicesService) { }

  fetchNews(search:string): Observable < any > {
    const url = 'https://newsapi.org/v2/everything?q='+ search +'&apiKey='+ environment.API_TOKEN;
    return this.baseService.get(url);
  }
  
}
