import { environment } from "src/environments/environment";

export const API_CONFIG = {

    ADMIN_LOGIN: environment.API_URL + 'login',
    ADMIN_OTP_VERIFY: environment.API_URL + 'verify-otp',
    PERSONAL_INFO: environment.API_URL + 'personal-information-api',
    INCOME_INFO: environment.API_URL + 'employement-api',
    CURRENT_STATUS: environment.API_URL + 'waiting-approval-api',
    KYC_UPLOAD: environment.API_URL + 'kyc-details-api',
    LANDING_CONTENT: environment.API_URL + 'title-content',
    EMI_STATUS: environment.API_URL + 'emi-calculation',

};

export const URL_CONFIG = {
    
};

export const COMMON_CONFIG = {
    CONTENT_TYPE: 'application/json',
    AUTHORIZATION_BEARER: 'Authorization',
    
};