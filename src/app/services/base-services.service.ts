import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BaseServicesService {

  constructor(private _http: HttpClient) {
  }

  public get(url: string): Observable < any > {
    return this._http.get<any>(url)
                     .pipe(catchError(this.handleError))
  }

  private handleError(error: Response) {
    if (error.status === 401) {
        console.log(error);
    }
    return throwError(error || 'Server error');
  }

}
