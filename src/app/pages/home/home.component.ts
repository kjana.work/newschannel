import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from 'src/app/services/admin-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  panellist = ['Apple','India','Cricket','Oscar 2019','Technology','International'];
  selectedpannel = '';
  panelwidth = 0;
  showloader = false;
  newsData ={
    articles: [],
    totalResults: 0,
  };
  
  constructor(private adminservices: AdminServiceService) { }

  ngOnInit(): void {
    this.setBrowserOnload();
    this.calculateInteresting("15:15:00","15:15:12");
  }

  setBrowserOnload() {
    this.selectedpannel = this.panellist[0];
    this.panelwidth = 100/this.panellist.length;
    this.getNews(this.panellist[0]);
  }

  panelChange(selected:any) {
    this.selectedpannel = selected;
    this.getNews(selected);
  }

  getNews(newstype:string) {
    this.showloader = true;
    this.newsData = {
      articles: [],
      totalResults: 0,
    };
    this.adminservices.fetchNews(newstype).subscribe((res) => {
      //console.log(res);
      this.newsData = res;
      this.showloader = false;
    }, (err) => {
      console.log(err);
    });
  }

  calculateInteresting(startdate:string,enddate:string)  {
    console.log("Starting Time: "+startdate);
    console.log("End Time: "+ enddate);
    var timeStart:any = new Date("01/01/2021 " + startdate);
    var timeEnd:any = new Date("01/01/2021 " + enddate);
    var difference = (timeEnd - timeStart)/1000;
    var countInteresting = 0;
    timeStart.setSeconds(new Date(timeStart).getSeconds() - 1);
    for (let i = 0; i <= difference; i++) {
      var t = new Date(timeStart);
      t.setSeconds(t.getSeconds() + 1);
      // console.log(t.toTimeString().split(' ')[0]);
      // console.log(this.unique_char(t.toTimeString().split(' ')[0]));
      if(this.unique_char(t.toTimeString().split(' ')[0]) <= 3) {
        countInteresting++;
      }
      timeStart = t;
    }
    console.log("Interesting count between "+startdate + " and " + enddate + " is : " + countInteresting);
  }
  unique_char(str1:string) {
    var str=str1;
    var uniql="";
    for (var x=0;x < str.length;x++) {
    if(uniql.indexOf(str.charAt(x))==-1) {
      uniql += str[x];    
    }
    }
    return uniql.length;  
  }

}
